package parsers

class PortParser {

    companion object {
        const val DEFAULT_PORT = 10080

        fun getPort(args: Array<String>): Int {
            val port: Int
            try {
                port = args[0].toInt()
            } catch (e: Exception) {
                println("Port parser error. Default port will be used: $DEFAULT_PORT")
                return DEFAULT_PORT
            }
            println("Port will be used: $port")
            return port
        }
    }
}