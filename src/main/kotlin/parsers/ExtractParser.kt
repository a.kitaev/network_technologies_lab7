package parsers

import java.nio.ByteBuffer

class ExtractParser {

    companion object {
        fun getDomainName(buffer: ByteBuffer): String {
            val domainName = StringBuilder()
            for (iter in 5 until (buffer.array()[4] + 5)) {
                domainName.append(buffer.array()[iter].toChar())
            }
            return domainName.toString()
        }

        fun getPort(buffer: ByteBuffer, offset: Int, length: Int): Int {
            val portBuilder = StringBuilder()
            for (iter in offset until (offset + length)) {
                val pl = if (buffer.array()[iter] < 0) {
                    ExtractParser.getCode(buffer.array()[iter].toInt())
                } else {
                    val plBuilder = StringBuilder(Integer.toString(buffer.array()[iter].toInt(), 2))
                    while (plBuilder.length < 8) {
                        plBuilder.insert(0, "0")
                    }
                    plBuilder.toString()
                }
                portBuilder.append(pl)
            }
            return Integer.parseInt(portBuilder.toString(), 2)
        }

        fun getIpAddress(buffer: ByteBuffer): String {
            val ipAddress = StringBuilder()
            for (iter in 4 until 8) {
                if (buffer.array()[iter] < 0) {
                    val code = getCode(buffer.array()[iter].toInt())
                    val ipItem = Integer.parseInt(code, 2)
                    ipAddress.append(ipItem)
                } else {
                    ipAddress.append(buffer.array()[iter])
                }

                if (iter != 7) {
                    ipAddress.append(".")
                }
            }
            return ipAddress.toString()
        }

        private fun getCode(num: Int): String {
            val plusBin = (-num) - 1
            val plBuilder = StringBuilder(plusBin.toString(2))
            while (plBuilder.length < 8) {
                plBuilder.insert(0, "0")
            }
            var pl = plBuilder.toString()
            pl = pl.replace('0', '2')
            pl = pl.replace('1', '0')
            pl = pl.replace('2', '1')
            return pl
        }
    }
}