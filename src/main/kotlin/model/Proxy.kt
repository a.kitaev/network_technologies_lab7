package model

import org.xbill.DNS.*
import parsers.ExtractParser
import java.net.InetSocketAddress
import java.nio.ByteBuffer
import java.nio.channels.*

class Proxy(
    private val lPort: Int
) : Thread("Proxy thread") {

    companion object {
        private const val PORT = 53
        private const val DEFAULT_BUFFER_SIZE = 131072
    }

    private lateinit var selector: Selector
    private lateinit var serverSocketChannel: ServerSocketChannel
    private lateinit var dnsKey: SelectionKey

    override fun run() {
        try {
            configureServerChannel()
            configureDatagramChannel()

            while (Thread.currentThread().isAlive) {
                doSomething()
                print(".")
            }
        } catch (e: Exception) {
            e.printStackTrace()
            selector.close()
            serverSocketChannel.close()
        }
    }

    private fun configureServerChannel() {
        selector = Selector.open()
        serverSocketChannel = ServerSocketChannel.open().apply {
            bind(InetSocketAddress("localhost", lPort))
            configureBlocking(false)
            register(selector, SelectionKey.OP_ACCEPT)
        }
    }

    private fun configureDatagramChannel() {
        val client = DatagramChannel.open().apply {
            configureBlocking(false)
        }

        dnsKey = client.register(selector, SelectionKey.OP_READ).apply {
            attach(Request())
        }

        client.connect(InetSocketAddress(ResolverConfig.getCurrentConfig().server(), PORT))
    }

    private fun doSomething() {
        var numOfKeys = 0
        try {
            numOfKeys = selector.select()
        } catch (e: Exception) {
            e.printStackTrace()
        } finally {
            if (0 == numOfKeys) {
                return
            }
        }

        val iter = selector.selectedKeys().iterator()
        while (iter.hasNext()) {
            try {
                val key = iter.next()
                key.acceptIfAcceptable()
                if (key.isReadable && !read(key)) {
                    continue
                }
                key.writeIfWritable(key)
            } catch (e: Exception) {
                println(e.localizedMessage)
            }
        }
        selector.selectedKeys().clear()
    }

    private fun SelectionKey.acceptIfAcceptable() {
        if (!this.isAcceptable) {
            return
        }

        val clientSocketChannel = serverSocketChannel.accept()
        println("accept ${clientSocketChannel.remoteAddress}")
        clientSocketChannel.configureBlocking(false)
        val clientKey = clientSocketChannel.register(selector, SelectionKey.OP_READ)
        val data = Metadata(clientKey, clientKey, true)
        clientKey.attach(data)
    }

    private fun read(key: SelectionKey): Boolean {
        if (key == dnsKey) {
            return readFromDns(key)
        }

        val channel = key.channel() as SocketChannel
        val data = key.attachment() as Metadata
        println("read ${data.serverKey} (serverKey)")
        val buffer = data.buffer
        val bytesNumber = channel.read(buffer)
        if (bytesNumber.isReadFail()) {
            key.interestOps(0)
            return false
        }

        if (data.setupMode) {
            when (data.step) {
                0 -> firstStep(key, data)
                1 -> secondStep(key, data, buffer)
            }
        } else {
            val otherKey = key.getOtherKey(data)
            val curOps = otherKey.interestOps()
            otherKey.interestOps(SelectionKey.OP_WRITE or curOps)
            if (!buffer.hasRemaining()) {
                val curOpsKey = key.interestOps()
                key.interestOps(curOpsKey and SelectionKey.OP_READ.inv())
            }
        }

        return true
    }

    private fun firstStep(key: SelectionKey, data: Metadata) {
        val response = ByteArray(2) {
            when (it) {
                0 -> 5
                else -> 0
            }
        }

        data.buffer = ByteBuffer.wrap(response)
        data.step++
        key.interestOps(SelectionKey.OP_WRITE)
    }

    private fun secondStep(key: SelectionKey, data: Metadata, buffer: ByteBuffer) {
        if (buffer.array().size < 3) {
            println("Problem with buffer length")
            return
        }

        when (buffer.array()[3].toInt()) {
            1 -> setupIPv4(key, data, buffer)
            3 -> setupDomainName(key, data, buffer)
            else -> throw UnsupportedOperationException("secondStep error with ${buffer.array()[3]}")
        }
    }

    private fun readFromDns(dnsKey: SelectionKey): Boolean {
        val datagramChannel = dnsKey.channel() as DatagramChannel
        val dnsRequest = this.dnsKey.attachment() as Request
        val buffer = ByteBuffer.allocate(DEFAULT_BUFFER_SIZE)
        datagramChannel.read(buffer)
        val message = Message(buffer.array())
        val answer = message.getSectionArray(Section.ANSWER)

        if (answer.isEmpty()) {
            println("Got no IP-addresses from DNS-resolver")
            return true
        }

        val ipAddress = answer[0].rdataToString()
        val domainName = answer[0].name.toString()
        val data = dnsRequest.requests[domainName]
        val response = ByteArray(data!!.domainNameLength) {
            when (it) {
                0 -> 5
                1 -> 0
                2 -> 0
                3 -> 3
                else -> data.buffer.array()[it]
            }
        }
        dnsKey.interestOps(SelectionKey.OP_READ)
        finishSetup(data.clientKey, data, ipAddress, data.serverPort, response)
        return true
    }

    private fun finishSetup(
        clientKey: SelectionKey,
        data: Metadata,
        ipAddress: String?,
        serverPort: Int,
        response: ByteArray
    ) {
        data.buffer = ByteBuffer.wrap(response)
        data.step++
        clientKey.interestOps(SelectionKey.OP_WRITE)

        val serverSocketChannel = SocketChannel.open(InetSocketAddress(ipAddress, serverPort))
        serverSocketChannel.configureBlocking(false)
        val serverKey = serverSocketChannel.register(selector, SelectionKey.OP_READ)

        data.serverKey = serverKey
        serverKey.attach(data)
        data.readyToReceiveData = true
    }


    private fun setupIPv4(key: SelectionKey, data: Metadata, buffer: ByteBuffer) {
        println("Got IPv4")
        val ipAddress = ExtractParser.getIpAddress(buffer)
        val port = ExtractParser.getPort(buffer, 8, 2)
        val response = ByteArray(10) {
            when (it) {
                0 -> 5
                1 -> 0
                2 -> 0
                3 -> 1
                else -> buffer.array()[it]
            }
        }
        finishSetup(key, data, ipAddress, port, response)
    }

    private fun setupDomainName(key: SelectionKey, data: Metadata, buffer: ByteBuffer) {
        println("Got Domain Name")
        val domainName = ExtractParser.getDomainName(buffer)
        val port = ExtractParser.getPort(buffer, (buffer.array()[4]) + 5, 2)
        data.domainNameLength = buffer.array()[4] + 5 + 2
        data.serverPort = port
        val dnsRequest = dnsKey.attachment() as Request
        dnsRequest.requests["$domainName."] = data
        dnsRequest.toSend.add("$domainName.")
        dnsKey.interestOps(SelectionKey.OP_WRITE)
        key.interestOps(0)
    }

    private fun SelectionKey.writeIfWritable(key: SelectionKey) {
        if (!this.isWritable) {
            return
        }

        if (key == dnsKey) {
            send(key)
            return
        }

        val channel = key.channel() as SocketChannel
        val data = key.attachment() as Metadata
        val buffer = data.buffer

        if (data.setupMode) {
            channel.write(buffer)
            buffer.clear()
            data.buffer = ByteBuffer.allocate(DEFAULT_BUFFER_SIZE)
            if (data.readyToReceiveData) {
                data.setupMode = false
            }
            key.interestOps(SelectionKey.OP_READ)
        } else {
            val otherKey = getOtherKey(data)
            buffer.flip()
            channel.write(buffer)
            buffer.flip()
            val curOps = otherKey.interestOps()
            otherKey.interestOps(curOps or SelectionKey.OP_READ)
            if (buffer.position() == 0) {
                val curOpsKey = key.interestOps()
                key.interestOps(curOpsKey and SelectionKey.OP_WRITE.inv())
            }
        }
    }

    private fun send(key: SelectionKey) {
        val datagramChannel = key.channel() as DatagramChannel
        val dnsRequest = dnsKey.attachment() as Request
        dnsRequest.toSend.forEach {
            try {
                val message = Message.newQuery(Record.newRecord(Name(it), Type.A, DClass.ANY))
                datagramChannel.write(ByteBuffer.wrap(message.toWire()))
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
        dnsRequest.toSend.clear()
        key.interestOps(SelectionKey.OP_READ)
    }

    private fun Int.isReadFail(): Boolean {
        return (-1 == this)
    }

    private fun SelectionKey.getOtherKey(data: Metadata): SelectionKey {
        return when (this == data.serverKey) {
            true -> data.clientKey
            false -> data.serverKey
        }
    }
}