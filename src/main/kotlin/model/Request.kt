package model

data class Request(
    var requests: MutableMap<String, Metadata> = mutableMapOf(),
    var toSend: MutableList<String> = mutableListOf()
)
