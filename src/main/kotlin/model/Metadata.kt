package model

import parsers.PortParser
import java.nio.ByteBuffer
import java.nio.channels.SelectionKey

data class Metadata(
    val clientKey: SelectionKey,
    var serverKey: SelectionKey,
    var setupMode: Boolean,
    var readyToReceiveData: Boolean = false,
    var step: Int = 0,
    var serverPort: Int = PortParser.DEFAULT_PORT,
    var domainNameLength: Int = 0,
    var buffer: ByteBuffer = ByteBuffer.allocate(DEFAULT_BUFFER_SIZE)
)