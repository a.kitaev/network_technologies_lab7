package launcher

import model.Proxy
import parsers.PortParser

fun main(args: Array<String>) {
    try {
        val port = PortParser.getPort(args)
        val proxy = Proxy(port)
        proxy.start()
    } catch (e: Exception) {
        e.printStackTrace()
    }
}